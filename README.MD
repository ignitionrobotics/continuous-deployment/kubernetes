This repository contains a base configuration for Continuous Deployment (CD) to a Kubernetes cluster.

⚠️ This repository **should not be cloned** it contains base configurations that can be customized for your project needs by using the [Kubernetes CD template](https://gitlab.com/ignitionrobotics/continuous-deployment/templates/kubernetes). ⚠️

# Kubernetes Configuration

## Kustomize

[Kustomize](https://kustomize.io/) is used extensively throughout this repository to configure Kubernetes resources. 

Kubernetes base resources are provided in their most minimal form and can be found in the `base` directory. A number of patches are provided so that specific configurations are added based on project needs.

Configuration of project-specific environments is performed by creating a deployment repository using the [Kubernetes CD template](https://gitlab.com/ignitionrobotics/continuous-deployment/templates/kubernetes), and configuring the different environments there.